import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './core/components/layouts/full-layout.component';
import { SimpleLayoutComponent } from './core/components/layouts/simple-layout.component';
import{ ManagerLayoutComponent } from'./core/components/manager-layout/manager-layout.component';

//CanActivate Guards
import { UserAuthGuard } from './core/guards/user-auth.guard';
import { ManagerAuthGuard } from './core/guards/manager-auth.gaurds';
import { from } from 'rxjs';

export const routes: Routes = [
	{
		path: '',
		redirectTo: 'pages/login',
		pathMatch: 'full',
		
		
		
	},

	{
		path: 'pages',
		component: SimpleLayoutComponent,
		data: {
			title: 'Pages'
		},

		children: [
			{
				path: '',
				loadChildren: './core/components/pages/pages.module#PagesModule',
				
			}
		]
	},
	{
		path: '',
		component: FullLayoutComponent,
		data: {
			title: 'Home'
		},

		children: [
			{
				path: 'users',
				loadChildren: './core/components/user/user.module',
				canActivate: [UserAuthGuard],
			}
			
		]
	},

	{
		path: '',
		component: ManagerLayoutComponent,
		data: {
			title: 'Home'
		},
		
		children: [
			{
				path: 'manager',
				loadChildren: './core/components/manager/manager.module',
				canActivate: [ManagerAuthGuard],
			}
			
		]
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
