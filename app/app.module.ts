import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Shared Bootstrape Module
import { SharedModule } from './shared.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '../../node_modules/@angular/common/http';
// Layouts
import { FullLayoutComponent } from './core/components/layouts/full-layout.component';
import { SimpleLayoutComponent } from './core/components/layouts/simple-layout.component';
import { ManagerLayoutComponent } from'./core/components/manager-layout/manager-layout.component';
//Toaster Module
import { ToastrModule } from 'ngx-toastr';
import { Toastr } from './core/plugins/toastr/toastr';

//Authentication
import { AuthenticationService } from './core/services/authentication.service';

//All Api Route
import { ApiRoutes } from './core/config/api-routes';

//All Gaurds
import { UserAuthGuard } from './core/guards/user-auth.guard';

//Other Helper Class
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { UserService } from './core/services/user.services';
import { ManagerService } from './core/services/manager.services';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent,
		FullLayoutComponent,
    SimpleLayoutComponent,
    ManagerLayoutComponent,


    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
			maxOpened: 1,
			autoDismiss: true
    }),
    SharedModule,
  ],
  bootstrap: [AppComponent],
	providers: [
		AuthenticationService,
		Toastr,
		ApiRoutes,
		{
			provide: LocationStrategy,
			useClass: HashLocationStrategy
		},
    UserAuthGuard,

	
    UserService,
    ManagerService,
	],
})
export class AppModule { }
