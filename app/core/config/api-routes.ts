import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiRoutes {
    public url:string;
    private baseUrl = environment.server.baseUrl;


    public default_error_message = 'Something went wrong, don\'t worry our engineers are fixing it right now.';

    get getBaseUrl(): string {
        return this.baseUrl;
       
    }

    get loginUrl() {
        return `${this.getBaseUrl}/Api/login`;
    }


    //working just need to add raised  by in codeigniter
    //  get localtravelform() {
    //     return `${this.getBaseUrl}/Api/LocalForm`;
    // }


    localtravelform() {
        // return `${this.getBaseUrl}/Api/LocalForm?${username}`;
        return `${this.getBaseUrl}/Api/LocalForm`;
    }

    //Profile Check 
     get checkUserProfile (){
        return `${this.getBaseUrl}/Api/CheckUserProfile`;
    }

    //Get Users Details 
    get getusersdetails (){
        return `${this.getBaseUrl}/Api/GetUsersDetails`;
    }


    //Get Users Profile Details 

     getusersProfiledetails (userID){
      
        return `${this.getBaseUrl}/Api/GetUsersProfileDetails?${userID}`;
    }
    //Update User Profile Funtions
    UpdateUsersProfile(userId){
        return `${this.getBaseUrl}/Api/EditUsersProfile?${userId}`; 
    }


    //Submit DomesticForm Funtions
    InsertDomesticForm(userId){
    
        return `${this.getBaseUrl}/Api/DomesticForm?${userId}`; 
    }

    get uploadDemo (){
      
        return `${this.getBaseUrl}/Api/UploadFileFxn`;
    }

// ********************************Manager Dashboard**************************************

    //Get All Domestic Travel (Table)
    get AllDomesticTravel ()
    {
        return `${this.getBaseUrl}/Api/GetAllDomesticTravel_Details`;
    }

    //Get All Domestic Travel (Table)
     GetByIDDomesticTravel(travelID)
    {
        return `${this.getBaseUrl}/Api/GetDomesticForm?${travelID}`;
    }
    
    //Approved  Domestic Travel (Table)
    ManagerApprovalDomestic(travelID)
    {
        return `${this.getBaseUrl}/Api/ApprovedDomesticTrip?${travelID}`;
    }

    //Reject Domestic Travel (Table)
    ManagerRejectDomestic(travelID)
    {
        return `${this.getBaseUrl}/Api/RejectDomesticTrip?${travelID}`;
    }
}
