import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../../node_modules/@angular/forms';
import { Storage } from '../../../utilities/storage';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { Toastr } from '../../../plugins/toastr/toastr';
import { Helper } from '../../../utilities/helpers';
import { UserService } from '../../../services/user.services';
import { CustomValidator } from '../../../utilities/validator/custom-validator';
declare var $: any;
@Component({
	templateUrl: 'userProfile.component.html',
	providers: [
	],
})


export class UserProfileComponent implements OnInit {
	EditProfileForm: FormGroup;
	submitted = false;
	email = '';
	emp_code='';
	report_manager='';
	company='';
	designation='';
	level='';
	name = 'Angular 4';
	url = '';




	onSelectFile(event) {
	  if (event.target.files && event.target.files[0]) {
		var reader = new FileReader();
		console.log(reader);
  
		reader.readAsDataURL(event.target.files[0]); // read file as data url
  
		// reader.onload = (event) => { // called once readAsDataURL is completed
		//   this.url = event.target.result;
		// }
	  }
	}
	public delete(){
	  this.url = null;
	}

	
	constructor(
		private formBuilder: FormBuilder,
		public authService: AuthenticationService,
		private router: Router,
		public storage: Storage,
        public toastr: Toastr,
        public userService: UserService,
	) { }

	ngOnInit() {
		this.CheckUserProfile();
		this.GetDetailsOfUsers();
		this._initEditForm();
		}


		_initEditForm() {

			this.EditProfileForm = this.formBuilder.group({
				
				firstname: [''],
				lastname: [''],
				gender:[''],
		
				passport_status:[''],
				passport_number: [''],
				citizenship: [''],
				passport_expiry:[''],
				passport_country:[''],
				passport_issuePlace:[''],
				birth_place:[''],
				
			});
		}

//Check Profile Funtion
		CheckUserProfile() {
			this.userService.CheckProfile().subscribe(
				(profileDetails) => {
					if(profileDetails==1)
					{
						console.log("Nothing in profile table")
						this.toastr.showWarning('Please Complete Your Profile')
					}
					else{
						console.log(profileDetails[0].passport_expiry)
					  this.EditProfileForm = this.formBuilder.group({
							'firstname' : [profileDetails[0].first_name],
							'lastname' : [profileDetails[0].last_name],
							'gender' : [profileDetails[0].gender],
							'passport_status' : [profileDetails[0].passport_status],
							'passport_number' : [profileDetails[0].passport_no],
							'citizenship' : [profileDetails[0].citizenship_country	],
							'passport_expiry' : [profileDetails[0].passport_expiry],
							'passport_country' : [profileDetails[0].passport_issue_country],
							'passport_issuePlace' : [profileDetails[0].place_of_passport_issued],
							'birth_place' : [profileDetails[0].birth_place],
				
						});
						

					}
					},
				(error) => {
					this.toastr.showError('error')
				}
			);
		}



// Get Users details
GetDetailsOfUsers() {

	this.userService.GetUsersDetails().subscribe(
		(details) => {
			this.emp_code = details[0].emp_code;
			this.email = details[0].email;
			this.report_manager = details[0].reporting_manager;
			this.company = details[0].company;
			this.designation = details[0].designation;
			this.level = details[0].level;
			},
		(error) => {
			this.toastr.showError('error')
		}
	);
}

//Submit Edit Profile Form

onSubmit() {
	this.submitted = true;
	// if (!this.EditProfileForm.valid) {
	// 	this.toastr.showError("Please validate the fields!");
	// 	return true;
	// }
	this.EditFormSubmit(this.EditProfileForm.value);
}
EditFormSubmit(formData) {
      
	this.userService.EditProfileForm(formData).subscribe(
		(data) => {
			console.log(data);
		
			this.toastr.showSuccess('Successfully Edit')
		// this.reset()
		},
		(error) => {
			this.toastr.showError('error')
		}
	);
}
		

	  }
