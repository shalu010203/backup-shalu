import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared.module';

import { UserComponent } from './user.component';
import { UserRoutingModule } from './user-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InitiateRequestComponent } from './InitiateTravelRequest/initiateRequest.component';
import { UploadBillComponent } from './UploadBills/uploadbill.component';
import { AnalyticsComponent } from './Analytics/analytics.component';
import { EditTripsComponent } from './EditTrips/editTrips.component';
import {UserProfileComponent} from './UserProfile/userProfile.component';
import {LocalComponent} from './localTravel/local.component';


@NgModule({
  imports: [
    SharedModule,
    UserRoutingModule,
  ],
  declarations: [
    UserComponent,
    DashboardComponent,
    InitiateRequestComponent,
    UploadBillComponent,
    AnalyticsComponent,
    EditTripsComponent,
    UserProfileComponent,
    LocalComponent
    
    
  ],
  providers: [ ],
})
export default class UserModule { }
