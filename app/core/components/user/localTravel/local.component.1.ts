import { Component, OnInit } from '@angular/core';
import { home } from '../model/home.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../../node_modules/@angular/forms';
import { Toastr } from '../../../plugins/toastr/toastr';
import { Helper } from '../../../utilities/helpers';
import { UserService } from '../../../services/user.services';
import { CustomValidator } from '../../../utilities/validator/custom-validator';
import { Storage } from '../../../utilities/storage';
import { Router } from '@angular/router';
declare var $: any;
@Component({
	templateUrl: 'local.component.html',
	providers: [
	],
})


export class LocalComponent implements OnInit {
	home= new home();
	
	dataarray = [];
	rate_per_km ='';

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		public storage: Storage,
        public toastr: Toastr,
				public userService: UserService,
	) { }

	ngOnInit() {
		this.dataarray.push(this.home);
		}

		add(){
			this.home= new home();
			this.dataarray.push(this.home);
			this.rate_per_km = "";
		  }

		  remove(index) {
			this.dataarray.splice(index);
		  }


		  LocalFormSubmit()
		  {
			
			  console.log(this.dataarray);
			//   for (let index = 0; index < this.dataarray.length; index++) {
			// 	  const element: any[] = this.dataarray[index]['depart']
			// 	  console.log(element);
				  
			//   }


			this.userService.localtravel(this.dataarray).subscribe(
				(data) => {
				console.log(data)
				
				},
				(error) => {
					this.toastr.showError('error')
				}
				);
		  }


		//    travelmode Change 
		get_travelmode(value)
		{	
			if (value == "Car") {
			this.home.rate_per_km =7;
				} 
			else if (value == "Bike") {
				this.home.rate_per_km =5;
				
				}
				else if (value == "Other") {

					this.home.rate_per_km =10;
					
					}else {
				alert("no value")
			}
		}







		onSelectedFile(event) {
			if (event.target.files.length) {
			  const file = event.target.files[0];
		
			  this.home.upload_bill = file;
			  console.log(this.home.upload_bill);
			  
			 
			}
		  }


		// fileEvent(value){
			
		// 	let filename=event.target.files[0].name;
			
		// 	this.home.upload_bill=filename;
		// 	console.log(this.home.upload_bill);

			// if(event.target.files.length > 0) 
			// {
			// 	let filename=event.target.files[0].name;
			// 	this.home.upload_bill=filename;
			// 	console.log(this.home.upload_bill);
				
			// //   console.log(event.target.files[0].name);
			// }
		// }
	  }
