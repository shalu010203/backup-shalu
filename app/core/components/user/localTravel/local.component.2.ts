import { Component, OnInit } from '@angular/core';
import { home } from '../model/home.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '../../../../../../node_modules/@angular/forms';
import { Toastr } from '../../../plugins/toastr/toastr';
import { Helper } from '../../../utilities/helpers';
import { UserService } from '../../../services/user.services';
import { CustomValidator } from '../../../utilities/validator/custom-validator';
import { Storage } from '../../../utilities/storage';
import { Router } from '@angular/router';
declare var $: any;
@Component({
	templateUrl: 'local.component.html',
	providers: [
	],
})


export class LocalComponent implements OnInit {
	
	localTravelForm: FormGroup;
	items: FormArray;
	rate_per_km ='';


	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		public storage: Storage,
        public toastr: Toastr,
				public userService: UserService,
	) { }

	ngOnInit() {
		this._initLocalForm();
		}
		_initLocalForm() {
			this.localTravelForm = this.formBuilder.group({
				
				depart_from: [''],
				profile:[''],
				items: this.formBuilder.array([ this.createItem() ])
			});
		}
		createItem(): FormGroup {
			return this.formBuilder.group({
				depart_from: '',
				profile: [''],
				profiledata:['']
			});
		  }
		  addItem(): void {
			this.items = this.localTravelForm.get('items') as FormArray;
			
			this.items.push(this.createItem());
		  }
		
		  remove(index) {
			this.items.removeAt(index);
		  }

	
		  get formdata() {
			return <FormArray>this.localTravelForm.get('items');
		  }

		  LocalFormSubmit()
		  {
		
			this.userService.localtravel(this.localTravelForm.value.items).subscribe(
				(data) => {
				console.log(data)
				
				},
				(error) => {
					this.toastr.showError('error')
				}
				);
		  }


		onFileChange(event:any, index:any) {
			// console.log(event.target.files[0]);
			// console.log(index);
			console.log(this.formdata.controls[index].patchValue({profiledata:event.target.files[0]}));
			
			// if (event.target.files.length > 0) {
			//   const file = event.target.files[0];
			
			// this.localTravelForm.get('profile').setValue(file);

			// }
		  }


	  }
