import { Component, OnInit } from '@angular/core';
import { home } from '../model/home.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '../../../../../../node_modules/@angular/forms';
import { Toastr } from '../../../plugins/toastr/toastr';
import { ApiRoutes } from '../../../config/api-routes';
import { Helper } from '../../../utilities/helpers';
import { UserService } from '../../../services/user.services';
import { CustomValidator } from '../../../utilities/validator/custom-validator';
import { Storage } from '../../../utilities/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';


declare var $: any;
@Component({
	templateUrl: 'local.component.html',
	providers: [
	],
})


export class LocalComponent implements OnInit {
	
	localTravelForm: FormGroup;
	items: FormArray;
	
	HT
	rate_per_km ='';


	constructor(
		private apiRoutes: ApiRoutes,
		private http: HttpClient,
		private formBuilder: FormBuilder,
		private router: Router,
		public storage: Storage,
		
        public toastr: Toastr,
				public userService: UserService,
	) { }

	ngOnInit() {
		this._initLocalForm();
		}


		//intialize form
		_initLocalForm() {
			
			this.localTravelForm = this.formBuilder.group({
				
				depart_from: [''],
				profile:[''],
				items: this.formBuilder.array([ this.createItem() ])
			});
		}

		//create array
		createItem(): FormGroup {
			return this.formBuilder.group({
				depart_from: '',
				profile: [''],
				// profiledata:['']
			});
		  }
		  
		  // add new feild
		  addItem(): void {
			this.items = this.localTravelForm.get('items') as FormArray;
			
			this.items.push(this.createItem());
		  }
		
		  //remove feild
		  remove(index) {
			this.items.removeAt(index);
		  }

		  
		  //get formdata
		  get formdata() {
			return <FormArray>this.localTravelForm.get('items');
		  }

		// on submit from
		  LocalFormSubmit()
		  {
			let formData = new FormData();
		//	console.log(this.localTravelForm.value.items[0].profile);
			for (let index = 0; index < this.localTravelForm.value.items.length; index++) {
				formData.append('profile[]', this.localTravelForm.value.items[index].profile);
				formData.append('depart_from[]', this.localTravelForm.value.items[index].depart_from);
			}
		
			this.http.post(this.apiRoutes.localtravelform(), (formData)).subscribe((data) =>{
				console.log("uploaded");
			}, (error) => {
				console.log("error");
			})
			// formData.forEach((value,key) =>{
			// 	console.log(value)
			// });
		
		  }

		  // on select file
		onFileChange(event:any, index:any) {
			
			this.localTravelForm.value.items[index].profile=event.target.files[0];

		
		
		  }


	  }
