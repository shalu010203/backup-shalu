import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../../node_modules/@angular/forms';
import { Toastr } from '../../../plugins/toastr/toastr';
import { Helper } from '../../../utilities/helpers';
import { UserService } from '../../../services/user.services';
import { CustomValidator } from '../../../utilities/validator/custom-validator';
import { Storage } from '../../../utilities/storage';
import { Router } from '@angular/router';
declare var $: any;
@Component({
	templateUrl: 'dashboard.component.html',
	providers: [
	],
})


export class DashboardComponent implements OnInit {
	
	localTravelForm: FormGroup;
	submitted = false;
	error: string;

  fileUpload = {status: '', message: '', filePath: ''};

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		public storage: Storage,
        public toastr: Toastr,
        public userService: UserService,
	) { }

	ngOnInit() {
		this._initLocalForm();
		}


		_initLocalForm() {
			this.localTravelForm = this.formBuilder.group({
				
				source: ['', Validators.required],
				destination: ['', Validators.required],
				client:['', Validators.required],
				profile:['', Validators.required],
				
			});
		}
		get f() { return this.localTravelForm.controls; }


		onSubmit() {
			this.submitted = true;
			if (!this.localTravelForm.valid) {
				this.toastr.showError("Please validate the fields!");
				return true;
			}
			this.LocalFormSubmit();
		}


		LocalFormSubmit() {
			const formData = new FormData();
			formData.append('source', this.localTravelForm.get('source').value);
			 formData.append('destination', this.localTravelForm.get('destination').value);
			 formData.append('client', this.localTravelForm.get('client').value);
			 formData.append('profile', this.localTravelForm.get('profile').value);
			 formData.forEach((value, key) => {
				console.log("key %s: value %s", key, value);
				})

			this.userService.localtravelUpload(formData).subscribe(
				(data) => {
					console.log(data);
				
					// this.toastr.showSuccess('Successfully Submit')
				// this.reset()
				},
				(error) => {
					this.toastr.showError('error')
				}
			);
		}
		onFileChange(event) {
			if (event.target.files.length > 0) {
			  const file = event.target.files[0];
			  console.log(file);
			  this.localTravelForm.get('profile').setValue(file);
			}
		  }

		reset() {
			this.localTravelForm.reset();
			this.submitted = false;
		}

	  }
