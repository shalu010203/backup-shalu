<!-- Initiate Travel Request -->
<div class="container-fluid">

    <div class="row">
      <!-- Heading -->
      <h4 class="h4Style">Initiate Travel Request</h4>

    </div>

    <!-- Third Section -->

<div class="row">
    <div class="col-12" >
  
        <div class="card text-center ">
            <div class="card-header heading_style ">
                Initiate Travel Request
            </div>
            <div class="card-body">
               
         
                    <div class="row">
                      <div class="col-12 navbox">
                        <nav>
                          <div class="nav nav-tabs nav-fill nav_fill_width" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Domestic</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">International</a>
                            
                          </div>
                        </nav>
                        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                           <!-- DOMESTIC TRAVEL FORM -->
                           <form class="row text-left">
                            <div class="col-12">
                              <h5 class="form_heading">Travel Details</h5>
                            </div>
                            <div class="col-6">
                               
                                    <div class="form-group row">
                                      <label for="staticEmail" class="col-sm-4 col-form-label">DEPARTURE FROM</label>
                                      <div class="col-sm-7">
                                        <input type="text" class="form-control"  placeholder="Select Departure Place ">
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">ARRIVAL AT</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control"  placeholder="Select Arrival Place">
                                        </div>
                                     </div>
          
                                     <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">TRAVEL PURPOSE</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control"  placeholder="Enter Travel Purpose">
                                        </div>
                                     </div>
          
                                                                           
                            </div>
          
                          
                            
          
                            <div class="col-6">
                               
                                    <div class="form-group row">
                                      <label for="staticEmail" class="col-sm-5 col-form-label">DEPARTURE DATE & TIME</label>
                                      <div class="col-sm-7">
                                        <input type="text" class="form-control"  placeholder="Enter Departure Date & Time">
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">ARRIVAL DATE & TIME</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control"  placeholder="Enter Arrival Date & Time">
                                        </div>
                                     </div>
          
                                     <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">TRAVEL MODE</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control"  placeholder="Select Travel Mode">
                                        </div>
                                     </div>
          
                            </div>
          
                            <div class="col-12">
                                <h5 class="form_heading">Other Details</h5>
                             </div>
          
          
                             <div class="col-6">
                               
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-5 col-form-label">ACCOMMODATION REQUIRED</label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control"  placeholder="Select Accommodation Preference">
                                  </div>
                                </div>
          
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">ADVANCE REQUIRED</label>
                                    <div class="col-sm-7">
                                      <input type="text" class="form-control"  placeholder="Select Advance Preference">
                                    </div>
                                  </div>
          
                                  
                                                  
                        </div>
          
                      
                        
          
                        <div class="col-6">
                           
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-5 col-form-label">CLIENT INFO</label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control"  placeholder="Enter Client Information">
                                  </div>
                                </div>
          
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">REMARKS</label>
                                    <div class="col-sm-7">
                                      <input type="text" class="form-control"  placeholder="Enter Remarks">
                                    </div>
                                </div>
          
                                

                                <div class="col-11 text-right">
             
                                  <button type="submit" class="btn basebtn"  (click)="openModal(template)">Submit</button>
                  
                                 </div>
          
                               
                        </div>                
          
                        </form>
                           

                          </div>
                          <!--END DOMESTIC TRAVEL FORM -->












                          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                              <form class="row text-left">
                                  <div class="col-12">
                                    <h5 class="form_heading">Travel Details</h5>
                                  </div>
                                  <div class="col-6">
                                     
                                          <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">DEPARTURE FROM</label>
                                            <div class="col-sm-7">
                                              <input type="text" class="form-control"  placeholder="Select Departure Place ">
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                              <label for="staticEmail" class="col-sm-4 col-form-label">ARRIVAL AT</label>
                                              <div class="col-sm-7">
                                                <input type="text" class="form-control"  placeholder="Select Arrival Place">
                                              </div>
                                           </div>
  
                                           <div class="form-group row">
                                              <label for="staticEmail" class="col-sm-4 col-form-label">TRAVEL PURPOSE</label>
                                              <div class="col-sm-7">
                                                <input type="text" class="form-control"  placeholder="Enter Travel Purpose">
                                              </div>
                                           </div>
  
                                                                                 
                                  </div>
  

                                  
                                  <div class="col-6">
                                     
                                          <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">DEPARTURE DATE & TIME</label>
                                            <div class="col-sm-7">
                                              <input type="text" class="form-control"  placeholder="Enter Departure Date & Time">
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                              <label for="staticEmail" class="col-sm-4 col-form-label">ARRIVAL DATE & TIME</label>
                                              <div class="col-sm-7">
                                                <input type="text" class="form-control"  placeholder="Enter Arrival Date & Time">
                                              </div>
                                           </div>
  
                                           <div class="form-group row">
                                              <label for="staticEmail" class="col-sm-4 col-form-label">TRAVEL MODE</label>
                                              <div class="col-sm-7">
                                                <input type="text" class="form-control"  placeholder="Select Travel Mode">
                                              </div>
                                           </div>
  
                                  </div>
  
                                  <div class="col-12">
                                      <h5 class="form_heading">Other Details</h5>
                                   </div>
  
  
                                   <div class="col-6">
                                     
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">ACCOMMODATION REQUIRED</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control"  placeholder="Select Accommodation Preference">
                                        </div>
                                      </div>
  
                                      <div class="form-group row">
                                          <label for="staticEmail" class="col-sm-4 col-form-label">ADVANCE REQUIRED</label>
                                          <div class="col-sm-7">
                                            <input type="text" class="form-control"  placeholder="Select Advance Preference">
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">PASSPORT NUMBER</label>
                                            <div class="col-sm-7">
                                              <input type="text" class="form-control"  placeholder="Select Advance Preference">
                                            </div>
                                          </div>
                                    </div>

                              <div class="col-6">
                                 
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">CLIENT INFO</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control"  placeholder="Enter Client Information">
                                        </div>
                                      </div>
  
                                      <div class="form-group row">
                                          <label for="staticEmail" class="col-sm-4 col-form-label">REMARKS</label>
                                          <div class="col-sm-7">
                                            <input type="text" class="form-control"  placeholder="Enter Remarks">
                                          </div>
                                      </div>
  
                                     
                              </div>
  
                                  <div class="col-11 text-right">
                                      <input type="button" class="btn basebtn" data-toggle="modal" data-target="#InternationalTravelDetails" value="Submit">
                                  </div>
                
                              </form>
                           <!-- INTERNATIONAL TRAVEL FORM -->
                          </div>
                          
                          
                        </div>
                      
                      </div>
                    </div>
                  
            </div>
            
            
          </div>
   
  
      
    </div>
  
  </div>
    

</div>


<!-- VIEW DETAILS FOR DOMESTIC TRAVEL -->

  <!-- <div class="modal fade " id="DomesticTravelDetails" tabindex="-1" role="dialog" aria-labelledby="DomesticTravelDetailsTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="DomesticTravelDetailsTitle">Trip Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn basebtn">Save changes</button>
          </div>
        </div>
      </div>
    </div>  -->


    <!-- VIEW DETAILS FOR DOMESTIC TRAVEL -->



    <!-- VIEW DETAILS FOR INTERNATIONAL TRAVEL -->

  <div class="modal fade " id="InternationalTravelDetails" tabindex="-1" role="dialog" aria-labelledby="InternationalTravelDetailsTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="InternationalTravelDetailsTitle">Trip Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <form class="row text-left">
                  <div class="col-12">
                    <h5 class="form_heading">Travel Details</h5>
                  </div>
                  <div class="col-6">
                     
                          <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">DEPARTURE FROM</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control"  placeholder="Select Departure Place ">
                            </div>
                          </div>
                          <div class="form-group row">
                              <label for="staticEmail" class="col-sm-4 col-form-label">ARRIVAL AT</label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control"  placeholder="Select Arrival Place">
                              </div>
                           </div>

                           <div class="form-group row">
                              <label for="staticEmail" class="col-sm-4 col-form-label">TRAVEL PURPOSE</label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control"  placeholder="Enter Travel Purpose">
                              </div>
                           </div>

                                                                 
                  </div>

                
                  

                  <div class="col-6">
                     
                          <div class="form-group row">
                            <label for="staticEmail" class="col-sm-5 col-form-label">DEPARTURE DATE & TIME</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control"  placeholder="Enter Departure Date & Time">
                            </div>
                          </div>
                          <div class="form-group row">
                              <label for="staticEmail" class="col-sm-5 col-form-label">ARRIVAL DATE & TIME</label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control"  placeholder="Enter Arrival Date & Time">
                              </div>
                           </div>

                           <div class="form-group row">
                              <label for="staticEmail" class="col-sm-5 col-form-label">TRAVEL MODE</label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control"  placeholder="Select Travel Mode">
                              </div>
                           </div>

                  </div>

                  <div class="col-12">
                      <h5 class="form_heading">Other Details</h5>
                   </div>


                   <div class="col-6">
                     
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-5 col-form-label">ACCOMMODATION REQUIRED</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control"  placeholder="Select Accommodation Preference">
                        </div>
                      </div>

                      <div class="form-group row">
                          <label for="staticEmail" class="col-sm-5 col-form-label">ADVANCE REQUIRED</label>
                          <div class="col-sm-7">
                            <input type="text" class="form-control"  placeholder="Select Advance Preference">
                          </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-5 col-form-label">FOOD PREFERENCE</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control"  placeholder="Veg and Non Veg">
                            </div>
                          </div>

                          
                                        
                    </div>


              <div class="col-6">
                 
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-5 col-form-label">CLIENT INFO</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control"  placeholder="Enter Client Information">
                        </div>
                      </div>

                      <div class="form-group row">
                          <label for="staticEmail" class="col-sm-5 col-form-label">REMARKS</label>
                          <div class="col-sm-7">
                            <input type="text" class="form-control"  placeholder="Enter Remarks">
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="staticEmail" class="col-sm-5 col-form-label">SPECIAL ASSISTANCE</label>
                          <div class="col-sm-7">
                            <input type="text" class="form-control"  placeholder="Enter Special Assistance">
                          </div>
                      </div>

                     
              </div> 
              
              

              <div class="col-12">
                  <h5 class="form_heading">Passport Details </h5>
               </div>


               <div class="col-6">
               
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-5 col-form-label">PASSPORT NUMBER</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control"  placeholder="Select Accommodation Preference">
                    </div>
                  </div>


                  <div class="form-group row">
                      
                      <div class="col-sm-12">
                          <button type="button" class="btn passport_btn">Edit Passport Details</button>
                      </div>
                    </div>

                                    
                </div>

                <div class="col-6">
               
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-5 col-form-label">VISA NUMBER</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control"  placeholder="Select Accommodation Preference">
                      </div>
                    </div>

                  </div>

              </form>
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn basebtn">Save changes</button>
          </div>
        </div>
      </div>
    </div> 


    <!-- VIEW DETAILS FOR INTERNATIONAL TRAVEL -->

    <ng-template #template>
      <div class="modal-header ">
        <h4 class="modal-title pull-left">Modal</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="modalRef.hide()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  
      <div class="modal-body ">
        <form  [formGroup]="DomesticProfileForm" (ngSubmit)="_onSubmit()" class="row text-left">
          <div class="col-12">
            <h5 class="form_heading">Travel Details</h5>
          </div>
          <div class="col-6">
             
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">DEPARTURE FROM</label>
                    <div class="col-sm-7">
                      <input type="text" formControlName="departure_from" class="form-control"  placeholder="Select Departure Place "  [ngClass]="{ 'is-invalid': submitted && f.departure_from.errors }" />
                      <div *ngIf="submitted && f.departure_from.errors" class="invalid-feedback">
                        <div *ngIf="f.departure_from.errors.required">Source is required</div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="staticEmail" class="col-sm-4 col-form-label">ARRIVAL AT</label>
                      <div class="col-sm-7">
                        <input type="text" formControlName="arrival_at" class="form-control"  placeholder="Select Arrival Place"  [ngClass]="{ 'is-invalid': submitted && f.arrival_at.errors }" />
                        <div *ngIf="submitted && f.arrival_at.errors" class="invalid-feedback">
                          <div *ngIf="f.arrival_at.errors.required">Source is required</div>
                        </div>
                      </div>
                   </div>

                   <div class="form-group row">
                      <label for="staticEmail" class="col-sm-4 col-form-label">TRAVEL PURPOSE</label>
                      <div class="col-sm-7">
                        <input type="text" formControlName="travel_purpose" class="form-control"  placeholder="Enter Travel Purpose"  [ngClass]="{ 'is-invalid': submitted && f.travel_purpose.errors }" />
                        <div *ngIf="submitted && f.travel_purpose.errors" class="invalid-feedback">
                          <div *ngIf="f.travel_purpose.errors.required">Source is required</div>
                        </div>
                      </div>
                   </div>

                                                         
          </div>

        
          

          <div class="col-6">
             
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">DEPARTURE DATE & TIME</label>
                    <div class="col-sm-7">
                      <input type="text" formControlName="departure_date" class="form-control"  placeholder="Enter Departure Date & Time"  [ngClass]="{ 'is-invalid': submitted && f.departure_date.errors }" />
                      <div *ngIf="submitted && f.departure_date.errors" class="invalid-feedback">
                        <div *ngIf="f.departure_date.errors.required">Source is required</div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="staticEmail" class="col-sm-4 col-form-label">ARRIVAL DATE & TIME</label>
                      <div class="col-sm-7">
                        <input type="text" formControlName="arrival_date" class="form-control"  placeholder="Enter Arrival Date & Time"  [ngClass]="{ 'is-invalid': submitted && f.arrival_date.errors }" />
                        <div *ngIf="submitted && f.arrival_date.errors" class="invalid-feedback">
                          <div *ngIf="f.arrival_date.errors.required">Source is required</div>
                        </div>
                      </div>
                   </div>

                   <div class="form-group row">
                      <label for="staticEmail" class="col-sm-4 col-form-label">TRAVEL MODE</label>
                      <div class="col-sm-7">
                        <input type="text" formControlName="travel_mode" class="form-control"  placeholder="Select Travel Mode"  [ngClass]="{ 'is-invalid': submitted && f.travel_mode.errors }" />
                        <div *ngIf="submitted && f.travel_mode.errors" class="invalid-feedback">
                          <div *ngIf="f.travel_mode.errors.required">Source is required</div>
                        </div>
                      </div>
                   </div>

          </div>

          <div class="col-12">
              <h5 class="form_heading">Other Travel</h5>
           </div>


           <div class="col-6">
             
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-4 col-form-label">ACCOMMODATION REQUIRED</label>
                <div class="col-sm-7">
                  <input type="text" formControlName="accommodation_required" class="form-control"  placeholder="Select Accommodation Preference"  [ngClass]="{ 'is-invalid': submitted && f.accommodation_required.errors }" />
                  <div *ngIf="submitted && f.accommodation_required.errors" class="invalid-feedback">
                    <div *ngIf="f.accommodation_required.errors.required">Source is required</div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                  <label for="staticEmail" class="col-sm-4 col-form-label">ADVANCE REQUIRED</label>
                  <div class="col-sm-7">
                    <input type="text" formControlName="advance_required" class="form-control"  placeholder="Select Advance Preference"  [ngClass]="{ 'is-invalid': submitted && f.advance_required.errors }" />
                    <div *ngIf="submitted && f.advance_required.errors" class="invalid-feedback">
                      <div *ngIf="f.advance_required.errors.required">Source is required</div>
                    </div>
                  </div>
                </div>
                                
      </div>

    
      

      <div class="col-6">
         
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-4 col-form-label">CLIENT INFO</label>
                <div class="col-sm-7">
                  <input type="text" formControlName="client_info" class="form-control"  placeholder="Enter Client Information"  [ngClass]="{ 'is-invalid': submitted && f.client_info.errors }" />
                  <div *ngIf="submitted && f.client_info.errors" class="invalid-feedback">
                    <div *ngIf="f.client_info.errors.required">Source is required</div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                  <label for="staticEmail" class="col-sm-4 col-form-label">REMARKS</label>
                  <div class="col-sm-7">
                    <input type="text" formControlName="remarks" class="form-control"  placeholder="Enter Remarks"  [ngClass]="{ 'is-invalid': submitted && f.remarks.errors }" />
                    <div *ngIf="submitted && f.remarks.errors" class="invalid-feedback">
                      <div *ngIf="f.remarks.errors.required">Source is required</div>
                    </div>
                  </div>
              </div>

             
      </div>

          <div class="col-11 text-right">
             
            


                <!-- <button type="submit" class="btn basebtn"  (click)="openModal(template)">Submit</button> -->

               




          </div>
                
                

      </form>
      </div>
    </ng-template>