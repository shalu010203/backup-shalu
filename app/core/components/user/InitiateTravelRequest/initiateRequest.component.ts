import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '../../../../../../node_modules/@angular/forms';
import { Toastr } from '../../../plugins/toastr/toastr';
import { Helper } from '../../../utilities/helpers';
import { UserService } from '../../../services/user.services';
import { CustomValidator } from '../../../utilities/validator/custom-validator';
import { Storage } from '../../../utilities/storage';
import { Router } from '@angular/router';
declare var $: any;
@Component({
	templateUrl: 'initiateRequest.component.html',
	providers: [
	],
})


export class InitiateRequestComponent implements OnInit {
	
	modalRef: BsModalRef;
	raw_data_form: FormGroup;
	DomesticProfileForm: FormGroup;
	submitted = false;
// Raw form data vales. 
	departure_from="";
	arrival_at: "";
	travel_purpose:"";
	departure_date:"";
	arrival_date: "";
	travel_mode:"";
	accommodation_required:"";
	advance_required:"";
	client_info:"";
	remarks:"";
	firstname="";
	lastname="";
	advance_money="";
	showval=false;
	
	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		public storage: Storage,
        public toastr: Toastr,
				public userService: UserService,
				private modalService: BsModalService,
	) { }

	ngOnInit() {
		 this._initDomesticForm();
		this._initDomesticRawForm();
		
		}

		//getting value of raw form data
		_initDomesticRawForm()
		{
			this.raw_data_form = this.formBuilder.group({
				domes_depart: ['', Validators.required],
				domes_arri: ['', Validators.required],
				domes_trav_purpose:['', Validators.required],
				domes_depart_date:['', Validators.required],
				domes_arri_date: ['', Validators.required],
				domes_travel_mode:['', Validators.required],
				domes_accom: ['', Validators.required],
				domes_adv:['', Validators.required],
				domes_client_info:['', Validators.required],
				domes_remarks:['', Validators.required],
				domes_upload:[''],
				domes_adv_money: ['']
			});
			

		}



		//getting value of domestic form data
		_initDomesticForm() {
			this.DomesticProfileForm = this.formBuilder.group({
				
				departure_from: ['', Validators.required],
				arrival_at: ['', Validators.required],
				travel_purpose:['', Validators.required],
				departure_date:['', Validators.required],
				arrival_date: ['', Validators.required],
				travel_mode:['', Validators.required],
				accommodation_required:['', Validators.required],
				advance_required: ['', Validators.required],
				advance_money:['', Validators.required],
				client_info:['', Validators.required],
				remarks:['', Validators.required],
				firstname:['', Validators.required],
				lastname:['', Validators.required],
				upload:['']
				
			});
		}
		get f() { return this.DomesticProfileForm.controls; }
		get ist() { return this.raw_data_form.controls; }

		_onSubmit() {
			
			this.submitted = true;
			if (!this.DomesticProfileForm.valid) {
				this.toastr.showError("Please validate the fields!");
				return true;
			}

			// this.LocalFormSubmit(this.DomesticProfileForm.value);
		}

		onFileChange(event) {
			if (event.target.files.length > 0) {
			  const file = event.target.files[0];
			
			let filename = file
			this.raw_data_form.value.domes_upload=file;
			
			  this.DomesticProfileForm.get('upload').setValue(file);
			}
		  }

		 openModal(template: TemplateRef<any>) {
		
			this.submitted = true;
		if (!this.raw_data_form.valid) {
			this.toastr.showError("Please validate the fields!");
			return true;
		} else{
			this.modalRef = this.modalService.show(
				template,
				Object.assign({}, { class: 'gray modal-xl' })
			  );
			
			let rawData= this.raw_data_form.value
			
			this.DomesticProfileForm.patchValue({
	
			departure_from: rawData.domes_depart, 
			arrival_at: rawData.domes_arri,
			travel_purpose: rawData.domes_trav_purpose,
			departure_date: rawData.domes_depart_date,
			arrival_date: rawData.domes_arri_date,
			travel_mode: rawData.domes_travel_mode,
			accommodation_required: rawData.domes_accom,
			advance_required: rawData.domes_adv,
			advance_money : rawData.domes_adv_money,
			client_info: rawData.domes_client_info,
			remarks: rawData.domes_remarks,
			upload:rawData.domes_upload,
			
			
			});
			
			this.getProfileData();
		}


		

	  

  }


//Getiing data from users profile

  getProfileData()
  {
	this.userService.GetUsersProfileDetails().subscribe(
		(data) => {
			this.DomesticProfileForm.patchValue({
			firstname: data[0].first_name,
			lastname: data[0].last_name,
		});
		this._onSubmit();
		},
		(error) => {
			this.toastr.showError('error')
		}
		);
  }


  DomesticFormSubmit()
  {
	let Formdata= this.DomesticProfileForm.value;
	

	const formData = new FormData();
	
			formData.append('departure_from', this.DomesticProfileForm.get('departure_from').value);
			 formData.append('arrival_at', this.DomesticProfileForm.get('arrival_at').value);
			 formData.append('travel_purpose', this.DomesticProfileForm.get('travel_purpose').value);
			 formData.append('departure_date', this.DomesticProfileForm.get('departure_date').value);
			 formData.append('arrival_date', this.DomesticProfileForm.get('arrival_date').value);
			 formData.append('travel_mode', this.DomesticProfileForm.get('travel_mode').value);
			 formData.append('accommodation_required', this.DomesticProfileForm.get('accommodation_required').value);
			 formData.append('advance_required', this.DomesticProfileForm.get('advance_required').value);
			 formData.append('client_info', this.DomesticProfileForm.get('client_info').value);
			 formData.append('remarks', this.DomesticProfileForm.get('remarks').value);
			 formData.append('upload', this.DomesticProfileForm.get('upload').value);
			 formData.append('firstname', this.DomesticProfileForm.get('firstname').value);
			 formData.append('lastname', this.DomesticProfileForm.get('lastname').value);
			 formData.append('advance_money', this.DomesticProfileForm.get('advance_money').value);
		
			//  formData.forEach((value, key) => {
			// 	console.log("key %s: value %s", key, value);
			// 	})

	this.userService.SubmitDomesticForm(formData).subscribe(
		(data) => {
			console.log(data);
			if (data==1) {
				this.toastr.showSuccess("Successfully Submitted")
				this.reset();
			} else {
				this.toastr.showError("Oops! Something went wrong")
			}
		},
		(error) => {
			this.toastr.showError('error')
		}
		);
  }

	reset() {
		this.raw_data_form.reset();
		this.modalRef.hide()
		this.submitted = false;
	}

	onSelectAccomm($event)
	{

		if ($event.target.value=="Required") {
			this.showval=true;
			
			
		} else if ($event.target.value=="NotRequired") {
			this.showval=false;
			let contVal=0;
			this.raw_data_form.controls['domes_adv_money'].setValue(contVal);
			console.log(this.raw_data_form.value.domes_adv_money)
			
		} 
			}
  
	  }
