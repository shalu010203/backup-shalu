import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InitiateRequestComponent } from './InitiateTravelRequest/initiateRequest.component';
import { UploadBillComponent } from './UploadBills/uploadbill.component';
import { AnalyticsComponent } from './Analytics/analytics.component';
import { EditTripsComponent } from './EditTrips/editTrips.component';
import {UserProfileComponent} from './UserProfile/userProfile.component';
import {LocalComponent} from './localTravel/local.component';

const routes: Routes = [
	{
		path: '',
		data: {
			title: 'User'
		},

		children: [
			{
			  path: 'dashboard',
			  component: DashboardComponent,
			  data: {
				title: 'Dashboard'
			  }
			},
			{
			  path: 'initiate',
			  component: InitiateRequestComponent,
			  data: {
				title: 'Travel Request'
			  }
			},
			{
			  path: 'uploads',
			  component: UploadBillComponent,
			  data: {
				title: 'uploadbill'
			  }
			},
			{
			  path: 'edit',
			  component: EditTripsComponent,
			  data: {
				title: 'editTrips'
			  }
			},
			{
			  path: 'analytics',
			  component: AnalyticsComponent,
			  data: {
				title: 'analytics'
			  }
			},
			{
			  path: 'profile',
			  component: UserProfileComponent,
			  data: {
				title: 'profile'
			  },
			},
			{
				path: 'local',
				component: LocalComponent,
				data: {
				  title: 'local'
				},

			  }




		  ]
	},
	

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UserRoutingModule { }
