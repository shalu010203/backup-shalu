import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../../node_modules/@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ManagerService } from '../../../services/manager.services';
import { Storage } from '../../../utilities/storage';
import { Toastr } from '../../../plugins/toastr/toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';



@Component({
	templateUrl: 'domestic.component.html',
	providers: [
	],
})


export class DomesticComponent implements OnInit {
  modalRef: BsModalRef;

    public TravelId: any;
    public TravelDetails: any;
    public approvalStatus: any;
    accommodation_required ='';
    advance_money ='';
    advance_required ='';
    arrival_at ='';
    arrival_date ='';
    client_info ='';
    departure_date ='';
    departure_from ='';
    file_upload ='';
    raised_by ='';
    remarks ='';
    req_tripId ='';
    travel_mode ='';
    travel_purpose ='';
    showval=false;
    approvalDomesticForm: FormGroup;
    showvalForAdvance=true;
    showvalForBill=true;
    rejectStatus: any;
    approvebtn=true;
    rejectbtn=true;
    appHeading=true;
    rejHeading=true;
	
	

    constructor(
        public route: ActivatedRoute,
        private router: Router,
        public storage: Storage,
        public toastr: Toastr,
        public managerService: ManagerService,
        private modalService: BsModalService,
        private formBuilder: FormBuilder,
		) { }

	ngOnInit() {
    this._initApprovalForm();
        this.route.params.subscribe(
            params =>
             {
                  this.TravelId = params.id 
                  console.log(this.TravelId)
                });

                this.managerService.getDomesticTravelById(this.TravelId).subscribe(
                  (college) => {
                   
                    this.TravelDetails = college;
                    this.accommodation_required =  this.TravelDetails[0].accommodation_required
                    this.advance_money = this.TravelDetails[0].advance_money
                    this.advance_required = this.TravelDetails[0].advance_required
                    this.arrival_at = this.TravelDetails[0].arrival_at
                    this.arrival_date = this.TravelDetails[0].arrival_date
                    this.client_info = this.TravelDetails[0].client_info
                    this.departure_date = this.TravelDetails[0].departure_date
                    this.departure_from = this.TravelDetails[0].departure_from
                    this.file_upload =  this.TravelDetails[0].file_upload
                    this.raised_by = this.TravelDetails[0].raised_by
                    this.remarks = this.TravelDetails[0].remarks
                    this.req_tripId = this.TravelDetails[0].req_tripId
                    this.travel_mode = this.TravelDetails[0].travel_mode
                    this.travel_purpose = this.TravelDetails[0].travel_purpose
                    if ( this.advance_required=="NotRequired") {
                      this.showvalForAdvance=false;
                      this.showvalForBill=false;
                    }else{
                      this.showvalForAdvance=true;
                      this.showvalForBill=true;
                    }
                    // if (this.file_upload= "nofile") {
                    //   this.showvalForBill=false;
                    // }else{
                    //   this.showvalForBill=true;
                    // }
                  },
                  (error) => {
                  
                  }
                );

               
    }

    _initApprovalForm() {
			this.approvalDomesticForm = this.formBuilder.group({
				
				optradio: ['', Validators.required],
        amount: ['', Validators.required],
        mangerRemarks: ['', Validators.required]
				
				
			});
		}
    
    openModal(template: TemplateRef<any>,event) {
      // this.modalRef = this.modalService.show(template, this.config);
      this.modalRef = this.modalService.show(
        template,
        Object.assign({}, { class: 'gray modal-md' })
      );

      if (event==1) {
        this.approvebtn=true;
        this.rejectbtn=false;
        this.rejHeading=false;
        this.appHeading=true;

      } else {
        this.approvebtn=false;
        this.rejectbtn=true;
        this.showvalForBill=false;
        this.appHeading=false;
        this.rejHeading=true;
        
        let rejectstatus=2
        this.approvalDomesticForm.controls['optradio'].setValue(rejectstatus);
      }


    }

    onItemChange(event){
    
      if (event.target.value==1) {
        this.showval=true;
        
      } else {
        this.showval=false;
        let approvestatus=1
        let amount=0
       this.approvalDomesticForm.controls['optradio'].setValue(approvestatus);
      this.approvalDomesticForm.controls['amount'].setValue(amount);

      }
        
   }

   ApproveForm()
   {

     console.log(this.approvalDomesticForm.value)
    this.managerService.ManagerApproval(this.TravelId,this.approvalDomesticForm.value).subscribe(
      (data) => {
              this.approvalStatus = data;
              console.log(this.approvalStatus)
              
              this.reset()
              this.router.navigate(['/manager/dashboard']);
            },
      (error) => {
      
      }
    );
   }


   reset() {
		this.approvalDomesticForm.reset();
		this.modalRef.hide()
	
	}

	  }
