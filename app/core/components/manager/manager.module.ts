import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared.module';

import { ManagerComponent } from './manager.component';
import { ManagerRoutingModule } from './manger-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DomesticComponent } from './viewRequest/domestic.component';


@NgModule({
  imports: [
    SharedModule,
    ManagerRoutingModule,
  ],
  declarations: [
    ManagerComponent,
    DashboardComponent,
    DomesticComponent
   
    
    
  ],
  providers: [ ],
})
export default class ManagerModule { }
