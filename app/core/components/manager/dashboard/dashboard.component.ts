import { Component, OnInit } from '@angular/core';
import { ManagerService } from '../../../services/manager.services';
import { Storage } from '../../../utilities/storage';
import { Toastr } from '../../../plugins/toastr/toastr';
import { Router } from '@angular/router';


declare var $: any;
@Component({
	templateUrl: 'dashboard.component.html',
	providers: [
	],
})


export class DashboardComponent implements OnInit {
	domes_table: Object;
	

	constructor(
		public storage: Storage,
		public toastr: Toastr,
		private router: Router,
		public managerService: ManagerService,
	

	) { }

	ngOnInit() {

		this._DomesticTravelDetails();
		
		}


		_DomesticTravelDetails()
		{
		
			this.managerService.GetDomesticTravelDetails().subscribe(
				(data) => {
					this.domes_table =data;
					// console.log(this.domes_table);
								},
				(error) => {
					this.toastr.showError('error')
				}
			);
		}

		getDomesticId(id)
		{
			alert(id)
		}
	  }
