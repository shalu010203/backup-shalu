import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerComponent } from './manager.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DomesticComponent } from './viewRequest/domestic.component';

const routes: Routes = [
	{
		path: '',
		
		data: {
			title: 'manager'
		},

		children: [
			{
			  path: 'dashboard',
			  component: DashboardComponent,
			  data: {
				title: 'Dashboard'
			  }
			},
			{
				path: 'view/:id',
				component: DomesticComponent,
				data: {
				  title: 'View'
				}
			  },
		]
	},
	

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManagerRoutingModule { }
