import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { Storage } from '../../utilities/storage';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { Toastr } from '../../plugins/toastr/toastr';
import { Helper } from '../../utilities/helpers';
import { UserService } from '../../services/user.services';
import { CustomValidator } from '../../utilities/validator/custom-validator';


@Component({
	templateUrl: 'login.component.html',
})
export class LoginComponent implements OnInit {
	public loginForm: FormGroup;
	public loading: boolean = false;
	public submitted: boolean = false;
	public error: any = '';
	public currentUser: any;

	constructor(
		private formBuilder: FormBuilder,
		public authService: AuthenticationService,
		private router: Router,
		public storage: Storage,
        public toastr: Toastr,
        public userService: UserService,
	
	) {
	}

    ngOnInit() {
		this._initLoginForm();
	
	}

	_initLoginForm() {
		this.loginForm = this.formBuilder.group({
			email: ['', [
				Validators.required,
				CustomValidator.email,
			]],
			password: ['', Validators.required]
		});
	}


	_onLogin() {
		this.submitted = true;
		if (!this.loginForm.valid) {
			this.toastr.showError("Please validate the fields!");
			return true;
		}
		this._login(this.loginForm.value);
	}

	_login(formData) {
        
		this.authService.login(formData).subscribe(
			(authUser) => {
					
                    this.userService.setCredentials(authUser);
						

                        this._navigateToRoute(authUser);
					
				
			},
			(error) => {
				this.toastr.showError('error')
			}
		);
	}
    _navigateToRoute(authUser) {
       
		if (authUser.role=="user") {
            this.router.navigate(['/users/dashboard']);
        } else if (authUser.role=="admin") {
            this.router.navigate(['/manager/dashboard']);
        }else{
            this.router.navigate(['/pages/login']);
        }
	}

  
}
