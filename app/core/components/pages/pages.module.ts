import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { PagesRoutingModule } from './pages-routing.module';
import { FormsModule, ReactiveFormsModule } from '../../../../../node_modules/@angular/forms';
import { Toastr } from '../../plugins/toastr/toastr';
import { SharedModule } from '../../../shared.module';

@NgModule({
	imports: [
		PagesRoutingModule,
		SharedModule,
		ReactiveFormsModule,
	],
	declarations: [
		LoginComponent,
	],
	providers: [
		Toastr
	]
})
export class PagesModule { }
