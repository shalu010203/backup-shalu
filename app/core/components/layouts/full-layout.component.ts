import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { AuthenticationService } from '../../services/authentication.service';
import { Storage } from '../../utilities/storage';


@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  providers: []
})
export class FullLayoutComponent implements OnInit {

  public authUser: any;
  public notifications: any = [];
  public unReadNotifications: any = [];
  public unReadNotificationClass: any = {
    box: 'pulse-glow-repeat-0-2',
    text: '',
  };
  public date: any = new Date();
  public year: any;

  constructor(
    public router: Router,
    private authService: AuthenticationService,
    public storage: Storage,

    ) {
      this.year = moment(this.date).format('YYYY');
  }

  ngOnInit(){
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
  });
  }



 
  Logout(){
    localStorage.removeItem("authUser");
    this.router.navigate(['/pages/login']);
  }

}










