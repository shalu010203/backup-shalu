import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiRoutes } from '../config/api-routes';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '../utilities/storage';
import { Helper } from '../utilities/helpers';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class UserService {

    public collegeDetails: any = null;
    // public currentUser: any;
    // public currentOwner: any;

    constructor(
        private http: HttpClient,
        private apiRoutes: ApiRoutes,
        private router: Router,
        public storage: Storage
    ) {
    }

    get currentUser(){
        return this.storage.get('authUser');
        
    }

    // get currentOwner(){
        
    //     let currentUser = this.storage.get('authUser');

    //     return Helper.getObjProp(currentUser, 'owner');
    // }

    setCredentials(authUser) {
        this.storage.set('authUser', authUser);
         
  
    }

//Working
    // localtravel(credentials,params = {}){
    //     let url = this.apiRoutes.localtravelform + Helper.getQueryParamsString(params);
    //         return this.http.post(url, {
    //             source: Helper.getObjProp(credentials, 'source'),
    //             destination: Helper.getObjProp(credentials, 'destination'),
    //             client: Helper.getObjProp(credentials, 'client'),
    //             purpose: Helper.getObjProp(credentials, 'purpose'),
    //                raised_by: this.currentUser.email,
    //         })
    //             .map(response => {
    //                 return response;
    //             });
    // }



//Working v1
    // localtravel(form) {
    //     return this.http.post(this.apiRoutes.localtravelform ,
    //         {  
    //             source: form. source,
    //             destination: form. destination,
    //             client: form. client,
    //             purpose: form. purpose,
    //             raised_by: this.currentUser.email,
    //         })
    //         .map((res: any) => {
    //             return res;
    //         });
    // }


//Local travel Form
    // localtravel(form) {
       
    //     let username =this.currentUser.username;
  
    //     return this.http.post(this.apiRoutes.localtravelform(username), form)
    //         .map((response) => {
    //             return response;
    //         });


    // }

    localtravelUpload(formData)
    {
        return this.http.post(this.apiRoutes.uploadDemo, formData)
        .map((response) => {
            return response;
        });

    }
// Check Profile Status

// CheckProfile() {
//     let userid =this.currentUser.id
//     console.log(userid)
//     return this.http.post(this.apiRoutes.checkUserProfile(userid))
//         .map((response) => {
//             return response;
//         });
// }



// Check Profile function to check profile is updated or not
    CheckProfile(): Observable<any> {
        let userid =this.currentUser.id
        return this.http.post(this.apiRoutes.checkUserProfile,
        {
        id: userid, 
    
        }
        );
    }

//Get user details funtion

    GetUsersDetails()
    {
        let userid = this.currentUser.id
        return this.http.post(this.apiRoutes.getusersdetails,
        {id: userid}
        );

    }

    //Edit profile form submit Api
    EditProfileForm(form)
    {
        let userid =this.currentUser.id
        return this.http.post(this.apiRoutes.UpdateUsersProfile(userid), form)
            .map((response) => {
                return response;
            });

    }


    //Get user details funtion

    GetUsersProfileDetails()
    {
        let userid = this.currentUser.id
      
        return this.http.get(this.apiRoutes.getusersProfiledetails(userid) )
        .map((response) => {
            return response;
        });;

    }

    //Domestic Travel Form Submit
    SubmitDomesticForm(form)
    {
        let userid = this.currentUser.id
        return this.http.post(this.apiRoutes.InsertDomesticForm(userid), form )
        .map((response) => {
            return response;
        });

    }
   
}