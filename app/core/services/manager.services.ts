import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiRoutes } from '../config/api-routes';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '../utilities/storage';
import { Helper } from '../utilities/helpers';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ManagerService {

    public collegeDetails: any = null;
    // public currentUser: any;
    // public currentOwner: any;

    constructor(
        private http: HttpClient,
        private apiRoutes: ApiRoutes,
        private router: Router,
        public storage: Storage
    ) {
    }

    get currentUser(){
        return this.storage.get('authUser');
        
    }

    setCredentials(authUser) {
        this.storage.set('authUser', authUser);
         
  
    }



    //ALL Domestic Travel Details Submit
    GetDomesticTravelDetails()
    {   
        return this.http.get(this.apiRoutes.AllDomesticTravel )
        .map((response) => {
            return response;
        });
    }
    
    //GET Domestic Travel Details By Id
    getDomesticTravelById(travelID)
    {
        return this.http.get(this.apiRoutes.GetByIDDomesticTravel(travelID) )
        .map((response) => {
            return response;
        });
    }


    //Approved Domestic Travel Details By Id
    ManagerApproval(travelID,form)
    {
        console.log(form);
        return this.http.post(this.apiRoutes.ManagerApprovalDomestic(travelID), form )
        .map((response) => {
            return response;
        });
    }

    //Re Domestic Travel Details By Id
    ManagerReject(travelID,form)
    {
     
        return this.http.post(this.apiRoutes.ManagerRejectDomestic(travelID), form )
        .map((response) => {
            return response;
        });
    }
}