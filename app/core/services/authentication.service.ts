import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiRoutes } from '../config/api-routes';
import 'rxjs/Rx';
import { Storage } from '../utilities/storage';
import { Location } from '../../../../node_modules/@angular/common';
import { Helper } from '../utilities/helpers';


// import { User } from '../model/user';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
   
    constructor(
        private http: HttpClient,
        private apiRoutes: ApiRoutes,
        public storage: Storage,
        public location: Location
    ) { }

    //Working Login Example using helper class
    login( credentials,params = {}) {
        let url = this.apiRoutes.loginUrl + Helper.getQueryParamsString(params);
        return this.http.post(url, {
            email: Helper.getObjProp(credentials, 'email'),
            password: Helper.getObjProp(credentials, 'password'),
        
        })
            .map(authUser => {
                return authUser;
            });
    }




    // try one

    // login(credentials: Credentials): Observable<any> {
    //     let url = this.apiRoutes.loginUrl;
    //     return this.http.post(`${url}`,
    //       {'email': credentials.email, 'password': credentials.password}, {
    //     //   headers: new HttpHeaders({'Content-Type': 'application/json'}),
    //     //   observe: 'response'
    //     })
    //         .map(authUser => {
    //             return authUser;
    //         });
    // }




    // login(email: string, password: string) {
       
    //     return this.http.post<any>(`http://localhost/blogger/Api/login`, { email, password })
    //     .map(authUser => {
    //         // login successful if there's a jwt token in the response
           

    //         return authUser;
    //     });
    // }

    get authUser() {
        return this.storage.get('authUser');
    }

    // logout() {
       
    //     let formData = {
    //         fcm_token: this.storage.get('authUser').map(authUser => {
    //             // login successful if there's a jwt token in the response
               
    
    //             return authUser;
    //         })
    //     }
        
      
       
        // location.reload();
    // }
}