import { Injectable, ReflectiveInjector } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as strtr from 'locutus/php/strings/strtr';

@Injectable()
export class Helper {

	static processErrorResponse(error: any, toastr: any) {
		let error_messages;
		if(error.error){
			error_messages = Helper.getTransformedErrorResponse(error.error);
		}else{
			error_messages = Helper.getTransformedErrorResponse(error);
		}

		if (error_messages && error_messages.length > 0) {
			toastr.showError(error_messages[0]);
		} else {
			toastr.showError();
		}
	}

	static getQueryParamsString(paramsObj = {}) {
        let parts = [];
        for (var i in paramsObj) {
            if (paramsObj.hasOwnProperty(i)) {
                parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(paramsObj[i]));
            }
        }
        return '?' + parts.join("&");
    }

	static getTransformedErrorResponse(json_response: any) {
		if (!json_response) {
			return null;
		}

		if (Helper.hasValidationErrors(json_response)) {
			return Helper.getValidationErrorObj(json_response);
		} else if (typeof json_response === 'object' && json_response.hasOwnProperty('message')) {
			return [json_response.message];
		} else if (typeof json_response === "string") {
			return [JSON.parse(json_response).message];
		}
		return null;
	}

	static hasValidationErrors(error): boolean {
		if (error.code == 422) {
			return true;
		}
		return false;
	}

	/**
     * Get Translated String for the provided one
     * @param {string}  string
     * @param {array}   attributes
     */
    static getTranslatedStr(string, attributes = {}) {
		if (_.isString(string)) {
			return strtr(string, attributes);
		}
		return string;
	}

	static getValidationErrorObj(error) {
		let errorObj: any;
		errorObj = error;
		let error_messages = [];
		if (errorObj.errors) {
			let errorMessagesObj = errorObj.errors;
			for (let key in errorMessagesObj) {
				error_messages.push(errorMessagesObj[key])
			}
		}
		return (error_messages.length > 0) ? error_messages : ['Something went wrong, try again later!'];
	}

	static isEmpty(value) {
		let bool = value == null ||
			(typeof value === 'string' && value.length === 0) ||
			(Helper.isArray(value) && value.length === 0);

		if (typeof value === 'object') {
			for (let key in value) {
				if (value.hasOwnProperty(key))
					return false;
			}
			return true;
		}

		return bool;
	}

	static isInt(n) {
        return Number(n) === n && n % 1 === 0;
    }

    static isFloat(n) {
        return Number(n) === n && n % 1 !== 0;
    }

    static isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

	static isArray(value) {
		return Object.prototype.toString.call(value) === '[object Array]';
	}

	static isNull(value) {
		return _.isNull(value);
	}


	static ucfirst(str: string) {
		return str.replace(str[0], str[0].toUpperCase());
	}

	static objectExcept(obj, keys) {
		let target = {};

		for (var i in obj) {

			if (keys.indexOf(i) >= 0)
				continue;

			if (!Object.prototype.hasOwnProperty.call(obj, i))
				continue;

			target[i] = obj[i];
		}

		return target;
	}

	static getObjProp(obj: Object, dotNotationStr: string, defaultVal: any = null) {
		return _.get(obj, dotNotationStr, defaultVal);
	}

	static convertKeyToText(key) {
		const wordsArray = key.split('_');
		const text = (wordsArray.length > 0) ? wordsArray.map((word) => Helper.ucfirst(word)).join(' ') : Helper.ucfirst(key);
		return text;
	}


	static formatTimestamp(timestamp: string) {
		let formattedTimestamp = moment().format('DD/MM/YYYY, hh:mm A');
      	return formattedTimestamp;
	}

	static convertTimeTo24Hr(time12Hr: any) {
		// Check correct time format and split into components
		time12Hr = time12Hr.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time12Hr];

		if (time12Hr.length > 1) { // If time format correct
			time12Hr = time12Hr.slice(1); // Remove full string match value
			time12Hr[5] = +time12Hr[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
			time12Hr[0] = +time12Hr[0] % 12 || 12; // Adjust hours
		}
		return time12Hr.join(''); // return adjusted time or original string
	}

	static cloneObj(obj: Object, ...sources) {
		return _.assign({}, obj, ...sources);
	}

	static arrayDifference(firstArray, secondArray){
		return _.difference(firstArray, secondArray);
	  }

}
