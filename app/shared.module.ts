import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Ng bootstrap
import { BsDropdownModule, TabsModule, TooltipModule, ModalModule, BsDatepickerModule, TypeaheadModule, TimepickerModule, CollapseModule, AccordionModule } from 'ngx-bootstrap';


import { Storage } from './core/utilities/storage';
import { ControlMessages } from './core/utilities/validator/control-messages';
import { Toastr } from './core/plugins/toastr/toastr';
import { ToastrService } from 'ngx-toastr';
import { LoaderSpinner } from './core/directives/loader-spinner.directive';
import { LoadingSpinnerComponent } from './core/components/loading-spinner/loading-spinner.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		BsDropdownModule.forRoot(),
		TabsModule.forRoot(),
		TooltipModule.forRoot(),
		ModalModule.forRoot(),
		BsDatepickerModule.forRoot(),
		TypeaheadModule.forRoot(),
		TimepickerModule.forRoot(),
		AccordionModule.forRoot(),
		CollapseModule.forRoot(),
	],
	declarations: [
		ControlMessages,
		LoaderSpinner,
		LoadingSpinnerComponent,
	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		BsDropdownModule,
		TabsModule,
		TooltipModule,
		ModalModule,
		AccordionModule,
		BsDatepickerModule,
		TypeaheadModule,
		TimepickerModule,
		ControlMessages,
		CollapseModule,
	],
	providers: [
		Storage,
		Toastr,
		ToastrService,
	]
})
export class SharedModule {
}
